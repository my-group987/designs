// ignore_for_file: prefer_const_constructors, must_be_immutable

import 'package:flutter/material.dart';

class MyApp extends StatefulWidget {
   const MyApp({Key? key}) : super(key: key);
 
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  bool ispassword = true;
  @override
  Widget build(BuildContext context) {
     return Scaffold(
      appBar: AppBar(
        title:Text("LoginScreen\n                           By : Eng_Mohamed Khalid",
         style: TextStyle(
          color: Colors.amber,
        fontSize: 20
      ),
         ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 40 , right: 40 , left: 40, bottom: 20),
              child: Image.network("https://medcareppe.com/assets/login.PNG"),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 20 , left: 20 , bottom: 20),
              child: Text( "Welcome Back!",
              style: TextStyle(
                fontSize: 50,
                fontWeight: FontWeight.bold,
              ),
            ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 15 , left: 15,),
              child: TextFormField(
                decoration: InputDecoration(
                border: OutlineInputBorder(),
                  prefixIcon: Icon(Icons.person, color: Color.fromARGB(255, 54, 243, 33),),
                  label: Text("example@email.com",
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.blue,
                   ),
                  )
                ),
              ),
            ),
             SizedBox(height: 10,),
              Padding(
                padding: const EdgeInsets.only(right: 15 , left: 15,),
                child: TextFormField(
                  obscureText: ispassword,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    prefixIcon: Icon(Icons.lock,),
                    suffixIcon: IconButton(onPressed: (){
                     setState(() {
                       ispassword = !ispassword;
                     });
                    }, 
                    icon: Icon(ispassword ? Icons.visibility_off : Icons.visibility)) ,
                    label: Text("Password",
                    style: TextStyle(
                      fontSize: 20,
                      ),
                    )
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 160, right: 10, top: 10),
                child: Text("Forgot Password?",
                style: TextStyle(
                  fontSize: 20,
                ),
                ),
              ),
              Container(
                width: double.infinity,
                color: Colors.blue,
                margin: EdgeInsets.only(left: 20, right: 20 , top: 20 , bottom: 10),
                child: MaterialButton(
                  onPressed: (){},
                 child: Text("LOG IN",
                 style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                 ),
                 ),
                 ),
              ),
              Text("Or Connect With Social",
                style: TextStyle(
                  fontSize: 20,
                ),
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(right: 20, left: 30, top: 20),
                        width: 150,
                        color: Colors.blue,
                        child: MaterialButton(
                    onPressed: (){},
                   child: Text("Facebook",
                   style: TextStyle(
                    fontSize: 20,
                    color: Colors.white,
                   ),
                   ),
                   ),
                      ),
                    SizedBox(width: 15,),
                      
                   Container(
                    margin: EdgeInsets.only(right: 20, left: 20, top: 20),
                    width: 150,
                    color: Colors.red,
                     child: MaterialButton(
                      onPressed: (){},
                     child: Text("Googel",
                     style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                     ),
                     ),
                     ),
                   ),
                    ],
                  ),
                )
      
            
          ],
        ),
      ),
     );
  }
}

