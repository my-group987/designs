import 'package:flutter/material.dart';

class Meal extends StatefulWidget {
  Meal({Key? key}) : super(key: key);

  @override
  State<Meal> createState() => _MealState();
}

class _MealState extends State<Meal> {
  var Var = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Resturant"),
        centerTitle: true,
      ),
      body: ListView(
        children: [
          Padding(
             padding: const EdgeInsets.all(8.0),
             child: Image.network("https://th.bing.com/th/id/OIP.JR8r8odqpG56MvClgbSQgAHaHa?pid=ImgDet&w=959&h=960&rs=1"),
        ),
          Row(
            children: [
              Icon(Icons.keyboard_arrow_left ),
              SizedBox(width: 200,),
              Text(" Meals ",
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w600
              ),
              ),
              SizedBox(width: 15,),
              Image.network("https://th.bing.com/th/id/OIP.rfBw-Vv6E0HubMFlq-l85gHaE8?pid=ImgDet&rs=1",width: 100, height: 100,)
            ],
          ),
          Row(
            children: [
              Icon(Icons.keyboard_arrow_left ),
              SizedBox(width: 200,),
              Text(" Burger ",
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w600
              ),
              ),
              SizedBox(width: 15,),
              Image.network("https://th.bing.com/th/id/OIP.w7si89sfiJb-CnVANf1cYAAAAA?pid=ImgDet&w=300&h=300&rs=1",width: 100, height: 100,)
            ],
          ),
          Row(
            children: [
              Icon(Icons.keyboard_arrow_left ),
              SizedBox(width: 200,),
              Text(" Chicks ",
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w600
              ),
              ),
              SizedBox(width: 15,),
              Image.network("https://new3.co/wp-content/uploads/2020/02/8390-2.jpg",width: 100, height: 100,)
            ],
          ),
          Row(
            children: [
              Icon(Icons.keyboard_arrow_left ),
              SizedBox(width: 150,),
              Text(" Shawarma ",
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w600
              ),
              ),
              SizedBox(width: 15,),
              Image.network("https://th.bing.com/th/id/OIP.Xj0pagb0pLVVXecWDHMxuwHaEK?w=277&h=180&c=7&r=0&o=5&dpr=1.3&pid=1.7",width: 100, height: 100,)
            ],
          ),
          Row(
            children: [
              Icon(Icons.keyboard_arrow_left ),
              SizedBox(width: 200,),
              Text(" Mombar ",
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w600
              ),
              ),
              SizedBox(width: 15,),
              Image.network("https://th.bing.com/th/id/OIP.QbvxoMNCzN6FB6x59eBwOgHaEK?w=275&h=180&c=7&r=0&o=5&dpr=1.3&pid=1.7",width: 100, height: 100,)
            ],
          ),
          Row(
            children: [
              Icon(Icons.keyboard_arrow_left ),
              SizedBox(width: 70,),
              Text(" Chicken Shawarma ",
              style: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.w600
              ),
              ),
              SizedBox(width: 15,),
              Image.network("https://th.bing.com/th/id/OIP.7Sa9M9IAeXEnHVECCSkuiAHaEK?w=271&h=180&c=7&r=0&o=5&dpr=1.3&pid=1.7",width: 100, height: 100,)
            ],
          ),

        ],
      ),
      // bottomNavigationBar: BottomNavigationBar(
      //   type: BottomNavigationBarType.fixed,
      //      currentIndex: Var,
      //      onTap:(index){
      //       setState(() {
      //         Var = index;
      //       });
      //      },
      //    items: [

      //     BottomNavigationBarItem(
      //        label: "Home",
      //        icon: Icon(Icons.home , size: 40,), 
      //      ),
           
      //       BottomNavigationBarItem(
      //        label: "Pizza",
      //        icon: Icon(Icons.location_on , size: 40,)
      //      ),
      //      BottomNavigationBarItem(
      //        label: "Setting", 
      //        icon:Icon(Icons.settings , size: 40,)
      //      ),
           
          
      //    ],
      //     ),
    );
  }
}