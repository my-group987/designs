

import 'package:flutter/material.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  State<Register> createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  bool ispassword = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow,
      appBar: AppBar(
        backgroundColor: Colors.yellow,
      ),
     body: SingleChildScrollView(
       child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(top: 10, left: 10 , bottom: 15),
            child: CircleAvatar(
              maxRadius: 70,
              child: Icon(Icons.person_add , size: 70,
               ),
               ),
               ),
               Padding(
                 padding: const EdgeInsets.only(right:10, left: 10 ,),
                 child: TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text("First Name*"),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                    ) 
                  ),
                 ),
               ),
               SizedBox(height: 15,),
                Padding(
                 padding: const EdgeInsets.only(right:10, left: 10 ,),
                 child: TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text("Last Name*"),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                    ) 
                  ),
                 ),
               ),
               SizedBox(height: 15,),
                Padding(
                 padding: const EdgeInsets.only(right:10, left: 10 ,),
                 child: TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text("E-mail Address*"),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                    ) 
                  ),
                 ),
               ),
               SizedBox(height: 15,),
                Padding(
                 padding: const EdgeInsets.only(right:10, left: 10 ,),
                 child: TextFormField(
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    label: Text("Mobile Number"),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                    ) 
                  ),
                 ),
               ),
               SizedBox(height: 15,),
                Padding(
                 padding: const EdgeInsets.only(right:10, left: 10 ,),
                 child: TextFormField(
                  obscureText: ispassword,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    suffixIcon: IconButton(onPressed: (){
                       setState(() {
                         ispassword = !ispassword;
                       });
                      }, 
                      icon: Icon( ispassword ? Icons.visibility_off : Icons.visibility   )
                      ) ,
                         label: Text("PassWord*"),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                    ) 
                  ),
                 ),
               ),
               SizedBox(height: 20,),
               Container(
                decoration: BoxDecoration(
                  color: Colors.purple,
                  borderRadius: BorderRadius.circular(10)
                  ),
                    child: MaterialButton(onPressed: (){},
                 child: Text("Register",
                 style: TextStyle(
                  color: Colors.white,
                  fontSize: 25,
                  fontWeight: FontWeight.bold
                 ),
                 ),
                 ),
               ),
               SizedBox(height: 10,),
          ],
       ),
     ),

    );
  }
}