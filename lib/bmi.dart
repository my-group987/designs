// ignore_for_file: non_constant_identifier_names, prefer_const_constructors
import 'package:flutter/material.dart';
class MyScreen extends StatefulWidget {
  const MyScreen({Key? key}) : super(key: key);
@override
  State<MyScreen> createState() => _MyScreenState();
}
 class _MyScreenState extends State<MyScreen> {
  bool isMale = true;
 double height = 120.0;
   int weight = 10;
  int age = 20;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(
         title: Text("BMICalculator\n                           By : Eng_Mohamed Khalid",
         style: TextStyle(
        fontSize: 20
      ),
         ),
          ),
      body:Column(
       children:
       [
         Expanded(
           child: Padding(
             padding: const EdgeInsets.all(20.0),
             child: Row(
               children: [
                 Expanded(
                   child: GestureDetector(
                     onTap: () {
                       setState(() {
                         isMale = true;
                       });
                     },
                     child: Container(
                       child: Column(
                         mainAxisAlignment: MainAxisAlignment.center,
                         // ignore: prefer_const_literals_to_create_immutables
                         children: [
                           Icon( Icons.male , size: 60,),
                           SizedBox(
                             height: 15.0,
                           ),
                           Text(
                             'MALE',
                             style: TextStyle(
                               fontSize: 25.0,
                               fontWeight: FontWeight.bold,
                             ),
                           ),
                         ],
                       ),
                       decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(
                           10.0,
                         ),
                          color: isMale ? Colors.blue : Colors.grey,

                       ),
                     ),
                   ),
                 ),
                 SizedBox(
                   width: 20.0,
                 ),
                 Expanded(
                   child: GestureDetector(
                     onTap: () {
                       setState(() {
                         isMale = false;
                       });
                     },
                     child: Container(
                       child: Column(
                         mainAxisAlignment: MainAxisAlignment.center,
                         // ignore: prefer_const_literals_to_create_immutables
                         children: [
                           Icon( Icons.female , size: 60,),
                           SizedBox(
                             height: 15.0,
                           ),
                           Text(
                             'FEMALE',
                             style: TextStyle(
                               fontSize: 25.0,
                               fontWeight: FontWeight.bold,
                             ),
                           ),
                         ],
                       ),
                       decoration: BoxDecoration(
                         borderRadius: BorderRadius.circular(
                           10.0,
                         ),
                          color: isMale ? Colors.grey : Colors.blue,
                       ),
                     ),
                   ),
                 ),
               ],
             ),
           ),
         ),
         Expanded(
           child: Padding(
             padding: const EdgeInsets.symmetric(
               horizontal: 20.0,
             ),
             child: Container(
               child: Column(
                 mainAxisAlignment: MainAxisAlignment.center,
                 children: [
                   Text(
                     'HEIGHT',
                     style: TextStyle(
                       fontSize: 25.0,
                       fontWeight: FontWeight.bold,
                     ),
                   ),
                   Row(
                     crossAxisAlignment: CrossAxisAlignment.baseline,
                     mainAxisAlignment: MainAxisAlignment.center,
                     textBaseline: TextBaseline.alphabetic,
                     children: [
                       Text(
                         '${height.round()}',
                         style: TextStyle(
                           fontSize: 40.0,
                           fontWeight: FontWeight.w900,
                         ),
                       ),
                       SizedBox(
                         width: 5.0,
                       ),
                       Text(
                         'CM',
                         style: TextStyle(
                           fontSize: 20.0,
                           fontWeight: FontWeight.bold,
                         ),
                       ),
                     ],
                   ),
                   Slider(
                     value: height,
                     max: 220,
                     min: 50.0,
                     onChanged: (value) {
                       setState(() {
                         height = value;
                       });
                     },
                   ),
                 ],
               ),
               decoration: BoxDecoration(
                 borderRadius: BorderRadius.circular(
                   10.0,
                 ),
                 color: Colors.grey[400],
               ),
             ),
           ),
         ),
         Expanded(
           child: Padding(
             padding: const EdgeInsets.all(20.0),
             child: Row(
               children:
               [
                 Expanded(
                   child: Container(
                     child: Column(
                       mainAxisAlignment: MainAxisAlignment.center,
                       children: [
                         Text(
                           'WEIGHT',
                           style: TextStyle(
                             fontSize: 25.0,
                             fontWeight: FontWeight.bold,
                           ),
                         ),
                         Text(
                           '$weight',
                           style: TextStyle(
                             fontSize: 40.0,
                             fontWeight: FontWeight.w900,
                           ),
                         ),
                         Row(
                           mainAxisAlignment: MainAxisAlignment.center,
                           children: [
                             FloatingActionButton(
                               onPressed: () {
                                 setState(() {
                                   weight--;
                                 });
                               },
                               heroTag: 'weight-',
                               mini: true,
                               child: Icon(
                                 Icons.remove,
                               ),
                             ),
                             FloatingActionButton(
                               onPressed: () {
                                 setState(() {
                                   weight++;
                                 });
                               },
                               heroTag: 'weight+',
                               mini: true,
                               child: Icon(
                                 Icons.add,
                               ),
                             ),
                           ],
                         ),
                       ],
                     ),
                     decoration: BoxDecoration(
                       borderRadius: BorderRadius.circular(
                         10.0,
                       ),
                       color: Colors.grey[400],
                     ),
                   ),
                 ),
                 SizedBox(
                   width: 20.0,
                 ),
                 Expanded(
                   child: Container(
                     child: Column(
                       mainAxisAlignment: MainAxisAlignment.center,
                       children: [
                         Text(
                           'AGE',
                           style: TextStyle(
                             fontSize: 25.0,
                             fontWeight: FontWeight.bold,
                           ),
                         ),
                         Text(
                           '$age',
                           style: TextStyle(
                             fontSize: 40.0,
                             fontWeight: FontWeight.w900,
                           ),
                         ),
                         Row(
                           mainAxisAlignment: MainAxisAlignment.center,
                           children: [
                             FloatingActionButton(
                               onPressed: () {
                                 setState(() {
                                   age--;
                                 });
                               },
                               heroTag: 'age-',
                               mini: true,
                               child: Icon(
                                 Icons.remove,
                               ),
                             ),
                             FloatingActionButton(
                               onPressed: () {
                                 setState(() {
                                   age++;
                                 });
                               },
                               heroTag: 'age+',
                               mini: true,
                               child: Icon(
                                 Icons.add,
                               ),
                             ),
                           ],
                         ),
                       ],
                     ),
                     decoration: BoxDecoration(
                       borderRadius: BorderRadius.circular(
                         10.0,
                       ),
                       color: Colors.grey[400],
                     ),
                   ),
                 ),
               ],
             ),
           ),
         ),
         Container(
           width: double.infinity,
           color: Colors.blue,
           child: MaterialButton(
             onPressed: () {
               
             },
             height: 50.0,
             child: Text(
               'CALCULATE',
               style: TextStyle(
                 color: Colors.white,
               ),
             ),
           ),
         ),
       ],
     ),
       
    );
  }
}