// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';

class MyClass extends StatefulWidget {
  const MyClass({Key? key}) : super(key: key);

  @override
  State<MyClass> createState() => _MyClassState();
}

class _MyClassState extends State<MyClass> {
  bool ispassword = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
     appBar: AppBar(
      title:Text("SigninScreen\n                           By : Eng_Mohamed Khalid",
      style: TextStyle(
        color: Colors.black,
        fontSize: 20
      ),
      ),
     ),
       body: SingleChildScrollView(
         child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20 , right: 20 , left: 20, bottom: 20),
              child: Image.network("https://th.bing.com/th/id/OIP._90d7SM5bK7os4skQ09EDQAAAA?pid=ImgDet&rs=1"),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 20 , left: 20 , bottom: 20),
              child: Text( "SIGN IN",
                style: TextStyle(
                  fontSize: 40,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 15 , left: 15),
              child: TextFormField(
                  decoration: InputDecoration(
                  border: OutlineInputBorder(),
                    prefixIcon: Icon(Icons.person, color: Colors.blue,),
                    label: Text("example@email.com",
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.blue,
                     ),
                    )
                  ),
                ),
            ),
            SizedBox(height: 30,),
            Padding(
              padding: const EdgeInsets.only(right: 15 , left: 15),
              child: TextFormField(
                    obscureText: ispassword,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      prefixIcon: Icon(Icons.lock,),
                      suffixIcon: IconButton(onPressed: (){
                       setState(() {
                         ispassword = !ispassword ;
                       });
                      }, 
                      icon: Icon(ispassword ? Icons.visibility_off : Icons.visibility)) ,
                      label: Text("Password",
                      style: TextStyle(
                        fontSize: 20,
                        ),
                      )
                    ),
                  ),
            ),
                SizedBox(height: 20,),
            Container(
                width: double.infinity,
                color: Colors.blue,
                margin: EdgeInsets.only(left: 20, right: 20 , top: 20 , bottom: 10),
                child: MaterialButton(
                  onPressed: (){},
                 child: Text("SignIn",
                 style: TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                 ),
                 ),
                 ),
              ),
          ],
         ),
       ),
    );
  }
}

